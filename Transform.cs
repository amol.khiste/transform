﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using transform.Properties;

namespace transform
{
    public partial class Transform : Form
    {
        IntPtr _dialogHandle = IntPtr.Zero;
        Dictionary<string, Type> _controlStyles = null;

        public Transform()
        {
            InitializeComponent();

            _controlStyles = new Dictionary<string, Type>(StringComparer.InvariantCultureIgnoreCase)
            {
                {"STATIC", typeof(NativeMethods.StaticControlStyles)},
                {"BUTTON", typeof(NativeMethods.ButtonControlStyles)},
                {"EDIT", typeof(NativeMethods.EditControlStyles)},
                {"LISTBOX", typeof(NativeMethods.ListboxControlStyles)},
                {"COMBOBOX", typeof(NativeMethods.ComboboxControlStyles)},
            };
        }

        void Transform_Deactivate(object sender, EventArgs e)
        {
            Cursor = Cursors.Default;
        }

        private void selectDialogButton_MouseDown(object sender, MouseEventArgs e)
        {
            selectDialogButton.Capture = true;
            Cursor = Cursors.Cross;
        }

        private void selectDialogButton_MouseUp(object sender, MouseEventArgs e)
        {
            Cursor = Cursors.Default;
            selectDialogButton.Capture = false;
        }

        private void selectDialogButton_MouseMove(object sender, MouseEventArgs e)
        {
            if (selectDialogButton.Capture)
            {
                _dialogHandle = NativeMethods.WindowFromPoint(selectDialogButton.PointToScreen(e.Location));
                if (null != _dialogHandle)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Capacity = 255;
                    NativeMethods.RealGetWindowClass(_dialogHandle, sb, (UInt32)sb.Capacity);
                    string className = sb.ToString();
                    if (className == "#32770")
                    { 
                        windowClassValueLabel.Text = className + " (Dialog)";
                    }
                    else
                    {
                        windowClassValueLabel.Text = className;
                    }

                    // Allocate correct string length first
                    int length = NativeMethods.GetWindowTextLength(_dialogHandle);
                    sb = new StringBuilder(length + 1);
                    NativeMethods.GetWindowText(_dialogHandle, sb, sb.Capacity);
                    captionValueLabel.Text = sb.ToString();
                }
            }
        }

        private void parseBtn_Click(object sender, EventArgs e)
        {
            if (null != _dialogHandle)
            {
                IntPtr dlgCtrlHandle = NativeMethods.GetWindow(_dialogHandle, (int)NativeMethods.GetWindow_Cmd.GW_CHILD);
                StringBuilder sb = new StringBuilder();
                while (IntPtr.Zero != dlgCtrlHandle)
                {
                    int rowNum = controlsGridView.Rows.Add();
                    NativeMethods.GetClassName(dlgCtrlHandle, sb, 80);
                    string controlType = sb.ToString();
                    controlsGridView.Rows[rowNum].Cells["Type"].Value = controlType;
                    sb.Clear();
                    int dlgCtrlId = NativeMethods.GetDlgCtrlID(dlgCtrlHandle);
                    controlsGridView.Rows[rowNum].Cells["ID"].Value = dlgCtrlId;
                    int textLength = NativeMethods.SendMessage(dlgCtrlHandle, NativeMethods.WM_GETTEXTLENGTH, 0, 0);
                    sb.Capacity = textLength + 1;
                    NativeMethods.SendMessage(dlgCtrlHandle, NativeMethods.WM_GETTEXT, sb.Capacity, sb);
                    controlsGridView.Rows[rowNum].Cells["Text"].Value = sb.ToString();
                    sb.Clear();
                    IntPtr styles = NativeMethods.GetWindowLongPtr(dlgCtrlHandle, (int)NativeMethods.GWL.GWL_STYLE);
                    DataGridViewComboBoxCell stylesCell = controlsGridView.Rows[rowNum].Cells["Styles"] as DataGridViewComboBoxCell;
                    stylesCell.Items.AddRange(EnumerateWindowStyles(styles.ToInt64()).ToArray());

                    switch (controlType.ToUpper())
                    {
                        case "STATIC":
                        case "BUTTON":
                        case "COMBOBOX":
                            stylesCell.Items.AddRange(EnumerateControlStylesHack(styles.ToInt64(), controlType).ToArray());
                            break;
                        default:
                            stylesCell.Items.AddRange(EnumerateControlStyles(styles.ToInt64(), controlType).ToArray());
                            break;
                    }

                    dlgCtrlHandle = NativeMethods.GetWindow(dlgCtrlHandle, (int)NativeMethods.GetWindow_Cmd.GW_HWNDNEXT);
                }
            }
        }

        private IEnumerable<string> EnumerateWindowStyles(long styles)
        {
            foreach (long style in Enum.GetValues(typeof(NativeMethods.ChildWindowStyles)))
            {
                if ((style & styles) != 0)
                {
                    yield return Enum.GetName(typeof(NativeMethods.ChildWindowStyles), style);
                }
            }
        }

        private IEnumerable<string> EnumerateControlStyles(long styles, string controlType)
        {
            if (_controlStyles.ContainsKey(controlType))
            {
                foreach (long style in Enum.GetValues(_controlStyles[controlType]))
                {
                    if ((style & styles) == style)
                    {
                        yield return Enum.GetName(_controlStyles[controlType], style);
                    }
                }
            }
            else
            {
                yield return String.Format("'{0}' style not available!", controlType);
            }
        }

        private IEnumerable<string> EnumerateControlStylesHack(long styles, string controlType)
        {
            if (_controlStyles.ContainsKey(controlType))
            {
                foreach (long style in Enum.GetValues(_controlStyles[controlType]))
                {
                    if ((style & styles) == style)
                    {
                        if (((0xFF00 & style) == 0) && ((0x00FF & styles) == (0x00FF & style)) ||
                            ((0 != style) &&
                             ((((0xFF0F & style) == 0) && ((0x00F0 & styles) == (0x00F0 & style))) ||
                              (((0xFFF0 & style) == 0) && ((0x000F & styles) == style)) ||
                              (((0xF000 & style) == 0) && ((0x0FFF & styles) == (0x0FFF & style))) ||
                              (((0xF0FF & style) == 0) && ((0x0F00 & styles) == (0x0F00 & style))) ||
                              (((0x0FFF & style) == 0) && ((0xF000 & styles) == (0xF000 & style))))))
                        {
                            yield return Enum.GetName(_controlStyles[controlType], style);
                        }
                    }
                }
            }
            else
            {
                yield return String.Format("'{0}' style not available!", controlType);
            }
        }
    }
}
