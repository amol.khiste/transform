﻿namespace transform
{
    partial class Transform
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Transform));
            this.mainLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.finderToolLabel = new System.Windows.Forms.Label();
            this.captionLabel = new System.Windows.Forms.Label();
            this.windowClassLabel = new System.Windows.Forms.Label();
            this.controlsGridView = new System.Windows.Forms.DataGridView();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.captionValueLabel = new System.Windows.Forms.Label();
            this.selectDialogButton = new System.Windows.Forms.Button();
            this.windowClassValueLabel = new System.Windows.Forms.Label();
            this.parseBtn = new System.Windows.Forms.Button();
            this.Type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Styles = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Text = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TabOrder = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mainLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.controlsGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // mainLayoutPanel
            // 
            resources.ApplyResources(this.mainLayoutPanel, "mainLayoutPanel");
            this.mainLayoutPanel.Controls.Add(this.finderToolLabel, 0, 0);
            this.mainLayoutPanel.Controls.Add(this.captionLabel, 0, 1);
            this.mainLayoutPanel.Controls.Add(this.windowClassLabel, 0, 2);
            this.mainLayoutPanel.Controls.Add(this.controlsGridView, 2, 3);
            this.mainLayoutPanel.Controls.Add(this.treeView1, 0, 3);
            this.mainLayoutPanel.Controls.Add(this.captionValueLabel, 1, 1);
            this.mainLayoutPanel.Controls.Add(this.selectDialogButton, 1, 0);
            this.mainLayoutPanel.Controls.Add(this.windowClassValueLabel, 1, 2);
            this.mainLayoutPanel.Controls.Add(this.parseBtn, 3, 0);
            this.mainLayoutPanel.Name = "mainLayoutPanel";
            // 
            // finderToolLabel
            // 
            resources.ApplyResources(this.finderToolLabel, "finderToolLabel");
            this.finderToolLabel.Name = "finderToolLabel";
            // 
            // captionLabel
            // 
            resources.ApplyResources(this.captionLabel, "captionLabel");
            this.captionLabel.Name = "captionLabel";
            // 
            // windowClassLabel
            // 
            resources.ApplyResources(this.windowClassLabel, "windowClassLabel");
            this.windowClassLabel.Name = "windowClassLabel";
            // 
            // controlsGridView
            // 
            this.controlsGridView.AllowUserToOrderColumns = true;
            resources.ApplyResources(this.controlsGridView, "controlsGridView");
            this.controlsGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.controlsGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Type,
            this.Styles,
            this.ID,
            this.Text,
            this.TabOrder});
            this.mainLayoutPanel.SetColumnSpan(this.controlsGridView, 2);
            this.controlsGridView.Name = "controlsGridView";
            // 
            // treeView1
            // 
            resources.ApplyResources(this.treeView1, "treeView1");
            this.mainLayoutPanel.SetColumnSpan(this.treeView1, 2);
            this.treeView1.Name = "treeView1";
            // 
            // captionValueLabel
            // 
            resources.ApplyResources(this.captionValueLabel, "captionValueLabel");
            this.mainLayoutPanel.SetColumnSpan(this.captionValueLabel, 2);
            this.captionValueLabel.Name = "captionValueLabel";
            // 
            // selectDialogButton
            // 
            resources.ApplyResources(this.selectDialogButton, "selectDialogButton");
            this.mainLayoutPanel.SetColumnSpan(this.selectDialogButton, 2);
            this.selectDialogButton.Name = "selectDialogButton";
            this.selectDialogButton.UseVisualStyleBackColor = true;
            this.selectDialogButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.selectDialogButton_MouseDown);
            this.selectDialogButton.MouseMove += new System.Windows.Forms.MouseEventHandler(this.selectDialogButton_MouseMove);
            this.selectDialogButton.MouseUp += new System.Windows.Forms.MouseEventHandler(this.selectDialogButton_MouseUp);
            // 
            // windowClassValueLabel
            // 
            resources.ApplyResources(this.windowClassValueLabel, "windowClassValueLabel");
            this.mainLayoutPanel.SetColumnSpan(this.windowClassValueLabel, 2);
            this.windowClassValueLabel.Name = "windowClassValueLabel";
            // 
            // parseBtn
            // 
            resources.ApplyResources(this.parseBtn, "parseBtn");
            this.parseBtn.Name = "parseBtn";
            this.parseBtn.UseVisualStyleBackColor = true;
            this.parseBtn.Click += new System.EventHandler(this.parseBtn_Click);
            // 
            // Type
            // 
            resources.ApplyResources(this.Type, "Type");
            this.Type.Name = "Type";
            this.Type.ReadOnly = true;
            // 
            // Styles
            // 
            resources.ApplyResources(this.Styles, "Styles");
            this.Styles.Name = "Styles";
            // 
            // ID
            // 
            resources.ApplyResources(this.ID, "ID");
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            // 
            // Text
            // 
            resources.ApplyResources(this.Text, "Text");
            this.Text.Name = "Text";
            // 
            // TabOrder
            // 
            resources.ApplyResources(this.TabOrder, "TabOrder");
            this.TabOrder.Name = "TabOrder";
            this.TabOrder.ReadOnly = true;
            // 
            // Transform
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.mainLayoutPanel);
            this.Name = "Transform";
            this.Deactivate += new System.EventHandler(this.Transform_Deactivate);
            this.mainLayoutPanel.ResumeLayout(false);
            this.mainLayoutPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.controlsGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel mainLayoutPanel;
        private System.Windows.Forms.Label captionValueLabel;
        private System.Windows.Forms.Button selectDialogButton;
        private System.Windows.Forms.Label finderToolLabel;
        private System.Windows.Forms.Label captionLabel;
        private System.Windows.Forms.Label windowClassLabel;
        private System.Windows.Forms.Label windowClassValueLabel;
        private System.Windows.Forms.DataGridView controlsGridView;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.Button parseBtn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Type;
        private System.Windows.Forms.DataGridViewComboBoxColumn Styles;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Text;
        private System.Windows.Forms.DataGridViewTextBoxColumn TabOrder;
    }
}

