﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Drawing;

namespace transform
{
    static class NativeMethods
    {
        [DllImport("kernel32.dll")]
        public static extern uint GetLastError();
        [DllImport("user32.dll")]
        public static extern IntPtr WindowFromPoint(Point p);
        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        public static extern int GetWindowTextLength(IntPtr hWnd);
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int GetWindowText(IntPtr hWnd, StringBuilder lpString, int nMaxCount);
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int InternalGetWindowText(IntPtr hWnd, StringBuilder lpString, int nMaxCount);
        [DllImport("user32.dll")]
        public static extern uint RealGetWindowClass(IntPtr hwnd, [Out] StringBuilder pszType,
           uint cchType);
        [DllImport("user32.dll")]
        public static extern IntPtr GetNextDlgTabItem(IntPtr hDlg, IntPtr hCtl, bool bPrevious);
        [DllImport("user32.dll")]
        public static extern int GetDlgCtrlID(IntPtr hwndCtl);
        [DllImport("user32.dll", SetLastError = true)]
        public static extern uint GetDlgItemText(IntPtr hDlg, int nIDDlgItem,
           [Out] StringBuilder lpString, int nMaxCount);
        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        public static extern int GetClassName(IntPtr hWnd, StringBuilder lpClassName, int nMaxCount);
        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr GetWindow(IntPtr hWnd, uint uCmd);
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern int SendMessage(IntPtr hWnd, UInt32 Msg, int wParam, int lParam); //WM_GETTEXTLENGTH
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern IntPtr SendMessage(IntPtr hWnd, UInt32 Msg, int wParam, [Out] StringBuilder lParam);
        [DllImport("user32.dll", EntryPoint = "GetWindowLong")]
        private static extern IntPtr GetWindowLongPtr32(IntPtr hWnd, int nIndex);
        [DllImport("user32.dll", EntryPoint = "GetWindowLongPtr")]
        private static extern IntPtr GetWindowLongPtr64(IntPtr hWnd, int nIndex);
        // This static method is required because Win32 does not support
        // GetWindowLongPtr directly
        public static IntPtr GetWindowLongPtr(IntPtr hWnd, int nIndex)
        {
            if (IntPtr.Size == 8)
                return GetWindowLongPtr64(hWnd, nIndex);
            else
                return GetWindowLongPtr32(hWnd, nIndex);
        }

        public const int WM_GETTEXT = 0xD;
        public const int WM_GETTEXTLENGTH = 0xE;

        public enum GetWindow_Cmd : uint
        {
            GW_HWNDFIRST = 0,
            GW_HWNDLAST = 1,
            GW_HWNDNEXT = 2,
            GW_HWNDPREV = 3,
            GW_OWNER = 4,
            GW_CHILD = 5,
            GW_ENABLEDPOPUP = 6
        }

        public enum GWL
        {
            GWL_WNDPROC = (-4),
            GWL_HINSTANCE = (-6),
            GWL_HWNDPARENT = (-8),
            GWL_STYLE = (-16),
            GWL_EXSTYLE = (-20),
            GWL_USERDATA = (-21),
            GWL_ID = (-12)
        }

        public enum ChildWindowStyles : long
        {
            WS_BORDER = 0x00800000,
            WS_CAPTION = 0x00C00000,
            //WS_CHILD = 0x40000000,
            WS_CHILDWINDOW = 0x40000000,
            WS_CLIPCHILDREN = 0x02000000,
            WS_CLIPSIBLINGS = 0x04000000,
            WS_DISABLED = 0x08000000,
            WS_DLGFRAME = 0x00400000,
            WS_GROUP = 0x00020000,
            WS_HSCROLL = 0x00100000,
            WS_ICONIC = 0x20000000,
            //WS_MAXIMIZE = 0x01000000,
            //WS_MAXIMIZEBOX = 0x00010000,
            //WS_MINIMIZE = 0x20000000,
            //WS_MINIMIZEBOX = 0x00020000,
            WS_OVERLAPPED = 0x00000000,
            //WS_OVERLAPPEDWINDOW = WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_THICKFRAME | WS_MINIMIZEBOX | WS_MAXIMIZEBOX,
            //WS_POPUP = unchecked((int)0x80000000),
            //WS_POPUPWINDOW = WS_POPUP | WS_BORDER | WS_SYSMENU,
            WS_SIZEBOX = 0x00040000,
            //WS_SYSMENU = 0x00080000,
            WS_TABSTOP = 0x00010000,
            WS_THICKFRAME = 0x00040000,
            WS_TILED = 0x00000000,
            //WS_TILEDWINDOW = WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_THICKFRAME | WS_MINIMIZEBOX | WS_MAXIMIZEBOX,
            WS_VISIBLE = 0x10000000,
            WS_VSCROLL = 0x00200000
        }

        public enum StaticControlStyles : long
        {
            SS_LEFT = 0x00000000,
            SS_CENTER = 0x00000001,
            SS_RIGHT = 0x00000002,
            SS_ICON = 0x00000003,
            SS_BLACKRECT = 0x00000004,
            SS_GRAYRECT = 0x00000005,
            SS_WHITERECT = 0x00000006,
            SS_BLACKFRAME = 0x00000007,
            SS_GRAYFRAME = 0x00000008,
            SS_WHITEFRAME = 0x00000009,
            SS_USERITEM = 0x0000000A,
            SS_SIMPLE = 0x0000000B,
            SS_LEFTNOWORDWRAP = 0x0000000C,
            //#if(WINVER >= 0x0400)
            SS_OWNERDRAW = 0x0000000D,
            SS_BITMAP = 0x0000000E,
            SS_ENHMETAFILE = 0x0000000F,
            SS_ETCHEDHORZ = 0x00000010,
            SS_ETCHEDVERT = 0x00000011,
            SS_ETCHEDFRAME = 0x00000012,
            SS_TYPEMASK = 0x0000001F,
            //#endif /* WINVER >= 0x0400 */
            //#if(WINVER >= 0x0501)
            SS_REALSIZECONTROL = 0x00000040,
            //#endif /* WINVER >= 0x0501 */
            SS_NOPREFIX = 0x00000080, /* Don't do "&" character translation */
            //#if(WINVER >= 0x0400)
            SS_NOTIFY = 0x00000100,
            SS_CENTERIMAGE = 0x00000200,
            SS_RIGHTJUST = 0x00000400,
            SS_REALSIZEIMAGE = 0x00000800,
            SS_SUNKEN = 0x00001000,
            SS_EDITCONTROL = 0x00002000,
            SS_ENDELLIPSIS = 0x00004000,
            SS_PATHELLIPSIS = 0x00008000,
            SS_WORDELLIPSIS = 0x0000C000,
            SS_ELLIPSISMASK = 0x0000C000,
            //#endif /* WINVER >= 0x0400 */
        }

        public enum ButtonControlStyles : long
        {
            BS_PUSHBUTTON = 0x00000000,
            BS_DEFPUSHBUTTON = 0x00000001,
            BS_CHECKBOX = 0x00000002,
            BS_AUTOCHECKBOX = 0x00000003,
            BS_RADIOBUTTON = 0x00000004,
            BS_3STATE = 0x00000005,
            BS_AUTO3STATE = 0x00000006,
            BS_GROUPBOX = 0x00000007,
            BS_USERBUTTON = 0x00000008,
            BS_AUTORADIOBUTTON = 0x00000009,
            BS_PUSHBOX = 0x0000000A,
            BS_OWNERDRAW = 0x0000000B,
            BS_TYPEMASK = 0x0000000F,
            BS_LEFTTEXT = 0x00000020,
            //#if(WINVER >= 0x0400)
            BS_TEXT = 0x00000000,
            BS_ICON = 0x00000040,
            BS_BITMAP = 0x00000080,
            BS_LEFT = 0x00000100,
            BS_RIGHT = 0x00000200,
            BS_CENTER = 0x00000300,
            BS_TOP = 0x00000400,
            BS_BOTTOM = 0x00000800,
            BS_VCENTER = 0x00000C00,
            BS_PUSHLIKE = 0x00001000,
            BS_MULTILINE = 0x00002000,
            BS_NOTIFY = 0x00004000,
            BS_FLAT = 0x00008000,
            BS_RIGHTBUTTON = BS_LEFTTEXT
            //#endif /* WINVER >= 0x0400 */
        }

        public enum EditControlStyles : long
        {
            ES_LEFT = 0x0000,
            ES_CENTER = 0x0001,
            ES_RIGHT = 0x0002,
            ES_MULTILINE = 0x0004,
            ES_UPPERCASE = 0x0008,
            ES_LOWERCASE = 0x0010,
            ES_PASSWORD = 0x0020,
            ES_AUTOVSCROLL = 0x0040,
            ES_AUTOHSCROLL = 0x0080,
            ES_NOHIDESEL = 0x0100,
            ES_OEMCONVERT = 0x0400,
            ES_READONLY = 0x0800,
            ES_WANTRETURN = 0x1000,
            //#if(WINVER >= 0x0400)
            ES_NUMBER = 0x2000,
            //#endif /* WINVER >= 0x0400 */
        }

        public enum ListboxControlStyles : long
        {
            LBS_NOTIFY = 0x0001,
            LBS_SORT = 0x0002,
            LBS_NOREDRAW = 0x0004,
            LBS_MULTIPLESEL = 0x0008,
            LBS_OWNERDRAWFIXED = 0x0010,
            LBS_OWNERDRAWVARIABLE = 0x0020,
            LBS_HASSTRINGS = 0x0040,
            LBS_USETABSTOPS = 0x0080,
            LBS_NOINTEGRALHEIGHT = 0x0100,
            LBS_MULTICOLUMN = 0x0200,
            LBS_WANTKEYBOARDINPUT = 0x0400,
            LBS_EXTENDEDSEL = 0x0800,
            LBS_DISABLENOSCROLL = 0x1000,
            LBS_NODATA = 0x2000,
            //#if(WINVER >= 0x0400)
            LBS_NOSEL = 0x4000,
            //#endif /* WINVER >= 0x0400 */
            LBS_COMBOBOX = 0x8000,
            LBS_STANDARD = (LBS_NOTIFY | LBS_SORT | ChildWindowStyles.WS_VSCROLL | ChildWindowStyles.WS_BORDER)
        }

        public enum ComboboxControlStyles : long
        {
            CBS_SIMPLE = 0x0001,
            CBS_DROPDOWN = 0x0002,
            CBS_DROPDOWNLIST = 0x0003,
            CBS_OWNERDRAWFIXED = 0x0010,
            CBS_OWNERDRAWVARIABLE = 0x0020,
            CBS_AUTOHSCROLL = 0x0040,
            CBS_OEMCONVERT = 0x0080,
            CBS_SORT = 0x0100,
            CBS_HASSTRINGS = 0x0200,
            CBS_NOINTEGRALHEIGHT = 0x0400,
            CBS_DISABLENOSCROLL = 0x0800,
            //#if(WINVER >= 0x0400)
            CBS_UPPERCASE = 0x2000,
            CBS_LOWERCASE = 0x4000,
            //#endif /* WINVER >= 0x0400 */
        }
    }
}
